//: Playground - noun: a place where people can play

extension Person {
    func toString() -> String {
        return "\(String(describing: type(of: self)))(name: \(self.name))"
    }
}

class Company {
    var ceo: CEO?
    var productManager: ProductManager?
    
    init() {
        print("🔴 Company: init()")
    }
    
    deinit {
        print("✅ Company: deinit()")
    }
    
}

class Person {
    var name: String
    
    init(name: String) {
        self.name = name
        print("🔴 \(self.toString()): init()")
    }
    
    deinit {
        print("✅ \(self.toString()): deinit()")
    }
    
    func talk(to receiver: Person, message: String) {
        print("💬 \(self.toString()) talks to \(receiver.toString()): --- \(message)")
    }
}

class CEO: Person {
    weak var productManager: ProductManager?
    
    lazy var printCompany = { [weak self] in
        guard let pm = self?.productManager else {
            print("ℹ️ No product manager in company")
            return
        }
        pm.printCompanyInfo()
    }
    
    lazy var printProductManager = { [weak self] in
        guard let pm = self?.productManager else {
            print("ℹ️ No product manager in company")
            return
        }
        
        print("ℹ️ Product manager:\t \(pm.toString())")
    }
    
    lazy var printDevelopers = { [weak self] in
        guard let pm = self?.productManager else {
            print("ℹ️ No product manager in company")
            return
        }
        
        for i in 0..<pm.developers.count {
            if let developer = pm.developers[i] {
                print("ℹ️ Developer #\(i+1):\t \(developer.toString())")
            } else {
                print("ℹ️ Developer #\(i+1):\t nil")
            }
        }
    }
}

class ProductManager: Person {
    weak var ceo: CEO?
    var developers = [Developer?]()
    
    func printCompanyInfo() {
        print("")
        print("ℹ️ --- Company info ---")
        print("ℹ️ CEO:\t\t\t\t \(self.ceo?.toString() ?? "nil")")
        self.ceo?.printProductManager()
        self.ceo?.printDevelopers()
        print("ℹ️ --- End company info ---")
        print("")
    }
}

class Developer: Person {
    weak var productManager: ProductManager?
    
    func talkToPM(message: String) {
        guard let pm = productManager else {
            print("💬 No product manager in company")
            return
        }
        
        talk(to: pm, message: message)
    }
    
    func talkToCEO(message: String) {
        guard let pm = productManager else {
            print("💬 No product manager in company")
            return
        }
        
        guard let ceo = pm.ceo else {
            print("💬 No CEO in company")
            return
        }
        
        talk(to: ceo, message: message)
    }
    
    func findOtherDeveloper(name: String) -> Developer? {
        guard let productManager = self.productManager else {
            print("💬 No product manager in company")
            return nil
        }
        
        for i in 0..<productManager.developers.count {
            if let developer = productManager.developers[i], developer !== self, developer.name == name {
                return developer
            }
        }
        
        return nil
    }
    
    func talkToOtherDevelopers(message: String) {
        guard let productManager = self.productManager else {
            print("💬 No product manager in company")
            return
        }
        
        for i in 0..<productManager.developers.count {
            if let developer = productManager.developers[i], developer !== self {
                talk(to: developer, message: message)
            }
        }
    }
}

func createCompany() -> Company {
    let company = Company()
    let ceo: CEO? = CEO(name: "Stepan")
    let pm: ProductManager? = ProductManager(name: "Vlad")
    let developer1: Developer? = Developer(name: "Evgeny")
    let developer2: Developer? = Developer(name: "Ilya")
    // в диаграмме нет 3-го разработчика, но он полезен для тестов общения между разработчиками
    //let developer3: Developer? = Developer(name: "Oleg")
    
    company.ceo = ceo
    company.productManager = pm
    
    ceo?.productManager = pm
    
    pm?.ceo = ceo
    
    developer1?.productManager = pm
    developer2?.productManager = pm
    //developer3?.productManager = pm
    
    pm?.developers.append(developer1)
    pm?.developers.append(developer2)
    //pm?.developers.append(developer3)
    
    return company
}

func run() {
    let company = createCompany() // создаем компанию только внутри скопа
    let developer1 = company.productManager?.developers[0]
    let developer2 = company.productManager?.developers[1]
    
    print("")
    
    developer1?.talkToCEO(message: "CEO, повысь мне зарплату")
    developer2?.talkToCEO(message: "CEO, я хочу в отпуск")
    
    developer1?.talkToPM(message: "PM, дай мне новую задачу")
    developer2?.talkToPM(message: "PM, нам нужен еще один погромист")
    
    developer1?.talkToOtherDevelopers(message: "To all: Что за дичь в продакшене???")
    developer2?.talkToOtherDevelopers(message: "To all: Ты сам её закоммитил... ")
    if let sender = developer1, let receiver = sender.findOtherDeveloper(name: "Ilya") {
        sender.talk(to: receiver, message: "Only to Ilya: пшшш")
    }
    
    print("")
    
    company.ceo?.printProductManager()
    company.ceo?.printDevelopers()
    company.ceo?.printCompany()
}

run()
