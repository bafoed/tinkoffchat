//
//  RootAssembly.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

class RootAssembly {
    lazy var presentationAssembly: IPresentationAssembly = PresentationAssembly(serviceAssembly: self.serviceAssembly)
    lazy var serviceAssembly: IServicesAssembly = ServicesAssembly(coreAssembly: self.coreAssembly)
    lazy var coreAssembly: ICoreAssembly = CoreAssembly()
}
