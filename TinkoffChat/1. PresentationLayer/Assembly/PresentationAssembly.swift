//
//  PresentationAssembly.swift
//  TinkoffChat
//
//  Created by bf on 17/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

protocol IPresentationAssembly {
    func profileViewController() -> ProfileViewController
    func editProfileViewController() -> EditProfileViewController
    func chooseAvatarProfileViewController() -> ChooseAvatarProfileViewController
    func conversationListViewController() -> ConversationsListViewController
    func conversationViewController(conversation: IConversation) -> ConversationViewController
    func tinkoffGestureRecognizer() -> TinkoffGestureRecognizer
}

class PresentationAssembly: IPresentationAssembly {
    private let serviceAssembly: IServicesAssembly
    
    init(serviceAssembly: IServicesAssembly) {
        self.serviceAssembly = serviceAssembly
    }
    
    func profileViewController() -> ProfileViewController {
        let controller = ProfileViewController(presentationAssembly: self,
                                               userService: serviceAssembly.userService)
        
        return controller
    }
    
    func editProfileViewController() -> EditProfileViewController {
        let controller = EditProfileViewController(presentationAssembly: self,
                                                   userService: serviceAssembly.userService)
        return controller
    }
    
    func chooseAvatarProfileViewController() -> ChooseAvatarProfileViewController {
        let controller = ChooseAvatarProfileViewController(presentationAssembly: self,
                                                           imageSearchService: serviceAssembly.imageSearchService)
        return controller
    }
    
    func conversationListViewController() -> ConversationsListViewController {
        let controller = ConversationsListViewController(presentationAssembly: self,
                                                         conversationService: serviceAssembly.conversationService)
        return controller
    }
    
    func conversationViewController(conversation: IConversation) -> ConversationViewController {
        let controller = ConversationViewController(presentationAssembly: self,
                                                    conversationService: serviceAssembly.conversationService,
                                                    messageService: serviceAssembly.messageService,
                                                    communicationService: serviceAssembly.communcationService)
        controller.conversation = conversation
        return controller
    }
    
    func tinkoffGestureRecognizer() -> TinkoffGestureRecognizer {
        let gestureRecognizer = TinkoffGestureRecognizer()
        return gestureRecognizer
    }
}
