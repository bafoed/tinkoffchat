//
//  ConversationCell.swift
//  TinkoffChat
//
//  Created by bf on 04/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//


protocol ConversationCell {
    func configure(with configuration: IMessage)
}
