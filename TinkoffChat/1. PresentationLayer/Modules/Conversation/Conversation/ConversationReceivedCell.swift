//
//  ConversationReceivedCell.swift
//  TinkoffChat
//
//  Created by bf on 04/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

class ConversationReceivedCell: UITableViewCell, ConversationCell {
    // MARK: - UI Components
    // В целях инкапсуляции все ссылки отмечены приватными, для доступа необходимо использовать протокол конфигурации.
    @IBOutlet private weak var messageLabel: UILabel!
    
    // MARK: - Configuration
    func configure(with configuration: IMessage) {
        self.messageText = configuration.messageText
    }

    // MARK: - Properties
    private(set) var messageText: String = "" {
        didSet {
            messageLabel.text = messageText
        }
    }
}
