//
//  ConversationViewController.swift
//  TinkoffChat
//
//  Created by bf on 04/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit
import CoreData

class ConversationViewController: UIViewController {
    // Dependencies
    private let presentationAssembly: IPresentationAssembly
    private var conversationService: IConversationService
    private var messageService: IMessageService
    private let communicationService: ICommunicationService

    // MARK: - UI Components
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    private lazy var navigationBarLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 17.0)
        label.textColor = .black
        return label
    }()
    
    // MARK: - Cells
    private let sentCellIdentifier = String(describing: ConversationSentCell.self)
    private let receivedCellIdentifier = String(describing: ConversationReceivedCell.self)

    // MARK: - Properties
    public var conversation: IConversation!
    private var user: IUser?
    
    private var needToScrollToBottom = true
    enum OnlineState { case online, offline }
    private var sendButtonState: OnlineState = .offline
    private var navigationBarLabelState: OnlineState = .offline

    // MARK: - Lifecycle
    init(presentationAssembly: IPresentationAssembly, conversationService: IConversationService, messageService: IMessageService, communicationService: ICommunicationService) {
        self.presentationAssembly = presentationAssembly
        self.conversationService = conversationService
        self.messageService = messageService
        self.communicationService = communicationService
        
        super.init(nibName: "ConversationViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageService.delegate = self
        messageService.setConversationForTableView(conversation: conversation)
        
        conversationService.onlineDelegate = self
        conversationService.setConversationForOnlineDelegate(conversation: conversation)
        
        conversationService.updateConversationReadStatus(conversation: conversation, read: true)
        
        user = conversationService.getUser(in: conversation)
        navigationBarLabel.text = user?.name
        
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.titleView = navigationBarLabel
        
        tableView.register(UINib(nibName: sentCellIdentifier, bundle: nil), forCellReuseIdentifier: sentCellIdentifier)
        tableView.register(UINib(nibName: receivedCellIdentifier, bundle: nil), forCellReuseIdentifier: receivedCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        tableView.reloadData()
        scrollToBottom(animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if needToScrollToBottom {
            scrollToBottom(animated: false)
            needToScrollToBottom = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateOnlineStates()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        messageService.delegate = nil
        conversationService.onlineDelegate = nil
    }
    
    // MARK: - Private methods
    private func scrollToBottom(animated: Bool) {
        let count = messageService.numberOfRowsInSection(section: 0)
        if count == 0 { return }
        let indexPath = IndexPath(row: count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
    }
    
    private func updateOnlineStates() {
        DispatchQueue.main.async {
            self.updateSendButtonState()
            self.updateNavigationBarLabelState()
        }
    }
    
    private func updateSendButtonState() {
        guard let user = self.user, let messageText = self.messageField.text else { return }
        
        let newSendButtonState: OnlineState
        newSendButtonState = (self.communicationService.isOnline(user: user) && !messageText.isEmpty) ? .online : .offline
        
        if newSendButtonState != sendButtonState {
            sendButtonState = newSendButtonState
            self.animateSendButton(to: newSendButtonState)
        }
    }
    
    private func animateSendButton(to state: OnlineState) {
        let scaleFactor: CGFloat = 1.15 // во сколько раз увеличить кнопку
        let scaleUpDuration = 0.5 // время, за которое кнопка увеличится в размере
        let scaledDuration = 0.5 // время, которое кнопка пробудет в увеличенном состоянии
        let scaleDownDuration = 0.3 // время, за которое кнопка вернется в исходное состояние
        let transitionDuration = scaleUpDuration // время изменение цвета текста
        
        // увеличиваем размер кнопки
        UIView.animate(withDuration: scaleUpDuration) { [weak self] in
            self?.sendButton.transform = CGAffineTransform(scaleX: scaleFactor,
                                                           y: scaleFactor)
        }
        // одновременно с этим меняем цвет кнопки
        UIView.transition(with: sendButton,
                          duration: transitionDuration,
                          options: [.transitionCrossDissolve],
                          animations: { [weak self] in
                            if state == .online {
                                self?.sendButton.isEnabled = true
                                self?.sendButton.borderColor = self?.view.tintColor
                                self?.sendButton.titleLabel?.textColor = self?.view.tintColor
                            } else {
                                self?.sendButton.isEnabled = false
                                self?.sendButton.borderColor = .gray
                                self?.sendButton.titleLabel?.textColor = .gray
                            }
        }, completion: nil)
        
        // возвращаем размер в исходное состояние, не трогая цвет
        UIView.animate(withDuration: scaleDownDuration,
                       delay: scaleUpDuration + scaledDuration,
                       options: [],
                       animations: { [weak self] in self?.sendButton.transform = .identity },
                       completion: nil)
    }
    
    private func updateNavigationBarLabelState() {
        guard let user = self.user else { return }
        
        let newNavigatonBarLabelState: OnlineState
        newNavigatonBarLabelState = self.communicationService.isOnline(user: user) ? .online : .offline
        
        if newNavigatonBarLabelState != navigationBarLabelState {
            navigationBarLabelState = newNavigatonBarLabelState
            animateNavigationBarLabel(to: newNavigatonBarLabelState)
        }
    }
    
    private func animateNavigationBarLabel(to state: OnlineState) {
        let scaleFactor: CGFloat = 1.10 // во сколько раз увеличить текст
        let scaleDuration = 1.0 // время, за которое будет будет происходить увеличение
        let transitionDuration = scaleDuration // время изменение цвета текста
        
        UIView.animate(withDuration: scaleDuration) { [weak self] in
            if state == .online {
                self?.navigationBarLabel.transform = CGAffineTransform(scaleX: scaleFactor,
                                                                       y: scaleFactor)
            } else {
                self?.navigationBarLabel.transform = .identity
            }
        }
        
        UIView.transition(with: navigationBarLabel,
                          duration: transitionDuration,
                          options: [.transitionCrossDissolve],
                          animations: { [weak self] in
                            if state == .online {
                                self?.navigationBarLabel.textColor = UIColor(red:0.05, green:0.76, blue:0.00, alpha:1.0)
                            } else {
                                self?.navigationBarLabel.textColor = .black
                            }
        }, completion: nil)
    }
    
    // MARK: - Keyboard features
    private func hideKeyboard() {
        messageField.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
                tableView.contentInset = UIEdgeInsetsMake(keyboardSize.height, 0, 0, 0)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
                tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }
        }
    }
    
    // MARK: - View events
    @IBAction func sendButtonTap(_ sender: Any) {
        if let messageText = messageField.text, !messageText.isEmpty, let user = user {
            let message = Message(identifier: Message.generateMessageId(),
                                  text: messageText,
                                  direction: .sent,
                                  type: .textMessage,
                                  date: Date())
            
            communicationService.send(message, to: user)
            conversationService.addMessage(message: message, to: conversation)
            
            messageField.text = ""
            self.updateSendButtonState()
        }
    }
    
    @IBAction func messageFieldChanged(_ sender: UITextField) {
        self.updateSendButtonState()
    }
}

// MARK: - UITableViewDelegate

extension ConversationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        hideKeyboard()
    }
}

// MARK: - UITableViewDataSource

extension ConversationViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return messageService.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageService.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let message = messageService.object(at: indexPath) else {
            return UITableViewCell()
        }
        let cell: (ConversationCell & UITableViewCell)?
        
        if message.direction == .sent {
            cell = tableView.dequeueReusableCell(withIdentifier: sentCellIdentifier) as? ConversationSentCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: receivedCellIdentifier) as? ConversationReceivedCell
        }
        
        if let cell = cell {
            cell.configure(with: message)
            return cell
        } else {
            return UITableViewCell()
        }
    }
}


// MARK: - IMessageServiceDelegate

extension ConversationViewController: IMessageServiceDelegate {
    func willChangeContent() {
        tableView.beginUpdates()
    }
    
    func contentChanged(didChange anObject: Any, at indexPath: IndexPath?, for type: MessageStorageTableViewChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .update:
            guard let indexPath = indexPath else { return }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            tableView.moveRow(at: indexPath, to: newIndexPath)
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
    
    func didChangeContent() {
        tableView.endUpdates()
        scrollToBottom(animated: true)
        conversationService.updateConversationReadStatus(conversation: conversation, read: true)
    }
}

// MARK: - IConversationServiceOnlineDelegate

extension ConversationViewController: IConversationServiceOnlineDelegate {
    func didBecomeOnline() {
        self.updateOnlineStates()
    }
    
    func didBecomeOffline() {
        self.updateOnlineStates()
    }
}
