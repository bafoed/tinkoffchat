//
//  ConversationCellConfiguration.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IConversationCellConfiguration {
    var name: String { get set }
    var message: String? { get set }
    var date: Date? { get set }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}


struct ConversationCellConfiguration: IConversationCellConfiguration {
    var name: String
    var message: String?
    var date: Date?
    var online: Bool
    var hasUnreadMessages: Bool
}

