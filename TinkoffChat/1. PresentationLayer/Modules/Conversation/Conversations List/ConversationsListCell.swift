
//
//  ConversationsListCell.swift
//  TinkoffChat
//
//  Created by bf on 03/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

class ConversationsListCell: UITableViewCell {
    // MARK: - UI Components
    // В целях инкапсуляции все ссылки отмечены приватными, для доступа необходимо использовать протокол конфигурации.
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var lastMessageLabel: UILabel!
    @IBOutlet private weak var lastMessageDateLabel: UILabel!
    @IBOutlet private weak var avatarPlaceholderView: UIView!
    @IBOutlet private weak var avatarPlaceholderLetter: UILabel!
    @IBOutlet private weak var unreadMessageMark: UILabel!
    
    // MARK: - Configuration
    func configure(with configuration: IConversationCellConfiguration) {
        name = configuration.name
        message = configuration.message
        date = configuration.date
        online = configuration.online
        hasUnreadMessages = configuration.hasUnreadMessages
    }
    
    // MARK: - Properties
    private(set) var name: String? {
        didSet {
            nameLabel.text = name
            updateAvatarPlaceholder(for: name)
            updateUnreadMessageMarkColor(for: name)
        }
    }
    
    private(set) var message: String? {
        didSet {
            if message != nil {
                lastMessageLabel.text = message
                lastMessageLabel.textColor = UIColor.gray
            } else {
                lastMessageLabel.text = "No messages yet"
                lastMessageLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    private(set) var date: Date? {
        didSet {
            guard let date = date else {
                lastMessageDateLabel.text = ""
                return
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = NSCalendar.current.isDateInToday(date)
                ? "HH:mm"
                : "dd MMMM"
            lastMessageDateLabel.text = dateFormatter.string(from: date)
        }
    }
    
    private(set) var online: Bool = false {
        didSet {
            backgroundColor = online
                ? UIColor(red: 1.0, green: 1.0, blue: 0.95, alpha: 1.0)
                : UIColor.clear
        }
    }
    
    private(set) var hasUnreadMessages: Bool = false {
        didSet {
            let fontSize = lastMessageLabel.font.pointSize
            lastMessageLabel.font = UIFont.systemFont(ofSize: fontSize, weight: hasUnreadMessages ? .semibold : .regular)
            unreadMessageMark.isHidden = !hasUnreadMessages
        }
    }
    
    // MARK: - Private methods
    private func generateFlatColor(for name: String?) -> UIColor {
        if let name = name {
            return UIColor.Flat.random(seed: name.hashValue) // генерируем цвет в зависимости от имени
        } else {
            return UIColor.Flat.gray
        }
    }
    
    private func updateAvatarPlaceholder(for name: String?) {
        let firstLetter = String(name?.first ?? "-") // берем первую букву
        let color = generateFlatColor(for: name)
        avatarPlaceholderLetter.text = firstLetter
        avatarPlaceholderView.backgroundColor = color
    }
    
    private func updateUnreadMessageMarkColor(for name: String?) {
        unreadMessageMark.textColor = generateFlatColor(for: name)
    }
}
