//
//  ConversationsListViewController.swift
//  TinkoffChat
//
//  Created by bf on 03/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

class ConversationsListViewController: UIViewController {
    
    // Dependencies
    private let presentationAssembly: IPresentationAssembly
    private var conversationService: IConversationService

    // MARK: - UI Components
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Cells
    private let identifier = String(describing: ConversationsListCell.self)
    
    // MARK: - Properties
    lazy var onlineConversations = [IConversation]()
    
    // MARK: - Lifecycle
    init(presentationAssembly: IPresentationAssembly, conversationService: IConversationService) {
        self.presentationAssembly = presentationAssembly
        self.conversationService = conversationService
        super.init(nibName: "ConversationsListViewController", bundle: nil)
        
        navigationItem.title = "Tinkoff Chat"
        navigationItem.largeTitleDisplayMode = .always
        
        let profileButton = UIBarButtonItem()
        profileButton.image = UIImage(named: "UserIcon")!
        profileButton.target = self
        profileButton.action = #selector(self.profileButtonTap(_:))
        navigationItem.rightBarButtonItem = profileButton
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Chats", style: .plain, target: nil, action: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        conversationService.onlineConversationsDelegate = self
        tableView.reloadData()
    }
    
    // MARK: - Private methods
    @objc func profileButtonTap(_ sender: UIBarButtonItem) {
        let vc = self.presentationAssembly.profileViewController()
        self.present(vc, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension ConversationsListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return conversationService.onlineConversationsNumberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversationService.onlineConversationsNumberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ConversationsListCell,
            let conversation = conversationService.onlineConversationsObject(at: indexPath),
            let user = conversationService.getUser(in: conversation)
        else {
            return UITableViewCell()
        }
        
        let latestMessage = conversationService.getLatestMessage(in: conversation)
        let cellConfiguration = ConversationCellConfiguration(name: user.name,
                                                              message: latestMessage?.messageText,
                                                              date: latestMessage?.date,
                                                              online: conversation.online,
                                                              hasUnreadMessages: conversation.hasUnreadMessages)

        cell.configure(with: cellConfiguration)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ConversationsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let conversation = conversationService.onlineConversationsObject(at: indexPath) else { return }
        
        let viewController = presentationAssembly.conversationViewController(conversation: conversation)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ConversationsListViewController: IConversationServiceDelegate {
    func willChangeContent() {
        tableView.beginUpdates()
    }
    
    func contentChanged(didChange anObject: Any, at indexPath: IndexPath?, for type: ConversationStorageTableViewChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .update:
            guard let indexPath = indexPath else { return }
            tableView.reloadRows(at: [indexPath], with: .automatic)
        case .move:
            guard let indexPath = indexPath else { return }
            guard let newIndexPath = newIndexPath else { return }
            tableView.moveRow(at: indexPath, to: newIndexPath)
            tableView.reloadRows(at: [newIndexPath], with: .none)
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }

    }
    
    func didChangeContent() {
        tableView.endUpdates()
    }
}
