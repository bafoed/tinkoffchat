//
//  ChooseAvatarCell.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

class ChooseAvatarCell: UICollectionViewCell {
    // MARK: - UI components
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Properties
    var onPrepareForReuse: (() -> Void)?
    
    // MARK: - View methods
    override func awakeFromNib() {
        super.awakeFromNib()
        revertToDefault()
    }
    
    // отвечает за отмену загрузки картинки при быстрой прокрутке
    override func prepareForReuse() {
        super.prepareForReuse()
        revertToDefault()
        
        self.onPrepareForReuse?()
    }
    
    // MARK: - Public methods
    func revertToDefault() {
        avatarImage.contentMode = .scaleToFill
        avatarImage.image = UIImage(named: "AvatarPlaceholder")
        
        overlayView.isHidden = false
        activityIndicator.startAnimating()
    }
    
    // вызывается после загрузки картинки
    func handleImageLoad(_ image: UIImage) {
        avatarImage.contentMode = .center
        avatarImage.image = image
        
        overlayView.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    // вызывается при нажатии на ячейку
    func handleTap() {
        overlayView.isHidden = false
        activityIndicator.startAnimating()
    }
    
    // вызывается при ошибке, которая произошла после нажатия на ячейку
    func handleTapError() {
        overlayView.isHidden = true
        activityIndicator.stopAnimating()
    }
}
