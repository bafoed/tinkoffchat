//
//  ChooseAvatarProfileViewController.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

class ChooseAvatarProfileViewController: UIViewController {
    // Dependencies
    private let presentationAssembly: IPresentationAssembly
    private let imageSearchService: IImageSearchService

    
    // MARK: - UI components
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    private let activityIndicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicator.hidesWhenStopped = true
        return indicator
    }()
    
    
    // MARK: - Properties
    weak var delegate: ChooseAvatarProfileViewDelegate?
    private var searchResults = [IImageSearchResult]()
    private let avatarCell = String(describing: ChooseAvatarCell.self)
    
    
    // MARK: - Lifecycle
    init(presentationAssembly: IPresentationAssembly, imageSearchService: IImageSearchService) {
        self.presentationAssembly = presentationAssembly
        self.imageSearchService = imageSearchService
        super.init(nibName: "ChooseAvatarProfileViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.delegate = self
        
        collectionView.register(UINib(nibName: avatarCell, bundle: nil), forCellWithReuseIdentifier: avatarCell)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundView = activityIndicatorView
        
        activityIndicatorView.startAnimating()
        
        imageSearchService.search(with: "parrot",
                                  onSuccess: { [weak self] (results) in self?.handleAvatarSearch(results: results) },
                                  onError: { [weak self] (error) in self?.handleAvatarSearchError(error: error) })
        
    }
    
    @IBAction func cancelTap(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    // MARK: - Private methods
    private func handleAvatarSearch(results: [IImageSearchResult]) {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            self.searchResults = results
            self.collectionView.reloadData()
        }
    }
    
    private func handleAvatarSearchError(error: Error) {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            self.showAlert(title: "Ошибка при загрузке картинок", message: error.localizedDescription)
        }
    }
    
    private func showAlert(title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message ?? "Неизвестная ошибка", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


// MARK: - UICollectionViewDelegate
extension ChooseAvatarProfileViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ChooseAvatarCell else {
            return
        }
        
        cell.handleTap()
        
        let successHandler: (IDownloadedImage) -> Void = { [weak self] (downloadedImage) in
            DispatchQueue.main.async {
                self?.delegate?.avatarDidChange(image: downloadedImage.image)
                self?.dismiss(animated: true)
            }
        }
        
        let errorHandler: (Error) -> Void = { [weak cell, weak self] (error) in
            DispatchQueue.main.async {
                cell?.handleTapError()
                self?.showAlert(title: "Ошибка при выборе аватара", message: error.localizedDescription)
            }
        }
        
        let item = searchResults[indexPath.row]
        let _ = imageSearchService.download(image: item.full,
                                            onSuccess: successHandler,
                                            onError: errorHandler)
    }
}


// MARK: - UICollectionViewDataSource
extension ChooseAvatarProfileViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: avatarCell, for: indexPath) as? ChooseAvatarCell
        else {
            return UICollectionViewCell()
        }
        
        let successHandler: (IDownloadedImage) -> Void = { [weak cell] (downloadedImage) in
            DispatchQueue.main.async { cell?.handleImageLoad(downloadedImage.image) }
        }

        let item = searchResults[indexPath.row]
        let networkTask = imageSearchService.download(image: item.preview,
                                                      onSuccess: successHandler,
                                                      onError: { _ in })
        cell.onPrepareForReuse = { networkTask?.cancel() }
        
        return cell
    }
}


// MARK: - UICollectionViewDelegateFlowLayout
extension ChooseAvatarProfileViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 40) / 3
        return CGSize(width: cellWidth, height: cellWidth)
    }
}


// MARK: - UINavigationBarDelegate
extension ChooseAvatarProfileViewController: UINavigationBarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}
