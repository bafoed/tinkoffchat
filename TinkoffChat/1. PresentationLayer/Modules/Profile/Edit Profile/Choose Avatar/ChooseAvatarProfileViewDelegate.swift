//
//  ChooseAvatarProfileViewDelegate.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

protocol ChooseAvatarProfileViewDelegate: class {
    func avatarDidChange(image: UIImage)
}
