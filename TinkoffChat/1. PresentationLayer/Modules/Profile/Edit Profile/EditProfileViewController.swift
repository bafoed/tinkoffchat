//
//  EditProfileViewController.swift
//  TinkoffChat
//
//  Created by bf on 20/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit
import AVKit

class EditProfileViewController: UIViewController {
    
    // Dependencies
    private let presentationAssembly: IPresentationAssembly
    private let userService: IUserService

    // MARK: - UI components
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userBioTextField: UITextViewField!
    @IBOutlet weak var chooseAvatarButton: UIButton!
    @IBOutlet weak var saveViaCoreDataButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - Properties
    public var user: IUser?
    public weak var delegate: EditProfileViewDelegate?
    
    
    // MARK: - Private properties
    enum ChangedProperies { case name, about, avatar }
    
    private var savedConfiguration: IUser?
    private var changedProperties: [ChangedProperies: Bool] =
        [.name: false, .about: false, .avatar: false] {
        
        didSet {
            updateButtonsState()
        }
    }
    
    // MARK: - Lifecycle
    init(presentationAssembly: IPresentationAssembly, userService: IUserService) {
        self.presentationAssembly = presentationAssembly
        self.userService = userService
        super.init(nibName: "EditProfileViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()

        userNameTextField.delegate = self
        userBioTextField.delegate = self

        chooseAvatarButton.layer.cornerRadius = chooseAvatarButton.bounds.width / 2
        userAvatarImage.layer.cornerRadius = chooseAvatarButton.layer.cornerRadius
        
        setupHideKeyboardOnBackgroundTapGesture()
        updateUserInfo()
    }

    
    // MARK: - UI events methods
    @IBAction private func chooseAvatarButtonTap(_ sender: UIButton) {
        showUserAvatarChooseActionSheet()
    }
    
    @IBAction private func closeButtonTap(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func saveViaCoreDataButtonTap(_ sender: UIButton) {
        save()
    }
        
    
    // MARK: - Private methods
    private func save() {
        activityIndicator.startAnimating()
        self.updateButtonsState()
        
        let user = makeUserFromControls()
        userService.updateCurrentUser(currentUser: user, callback: handleDataManagerResponse)
    }
    
    private func handleDataManagerResponse(error: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            
            if error {
                self?.updateButtonsState()
                
                let retryAction = UIAlertAction(title: "Повторить", style: UIAlertActionStyle.default) { _ in
                    self?.save()
                }
                self?.showAlert(title: "Ошибка", message: "Ошибка при сохранении данных", actions: [retryAction])
                return
            }
            
            self?.user = self?.savedConfiguration
            self?.changedProperties[.name] = false
            self?.changedProperties[.about] = false
            self?.changedProperties[.avatar] = false
            self?.updateUserInfo()
            
            self?.showAlert(title: "Успешно!", message: "Данные успешно сохранены")
            if let user = self?.user {
                self?.delegate?.profileUpdated(user: user)
            }
        }
    }
    

    private func showAlert(title: String, message: String, actions: [UIAlertAction] = []) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    private func handleUserAvatarChoose(avatar: UIImage?) {
        if let avatar = avatar {
            self.userAvatarImage.image = avatar
            changedProperties[.avatar] = true
        }
    }

    private func makeUserFromControls() -> IUser {
        var user = User(identifier: "default",
                        name: "",
                        about: nil,
                        avatar: nil)
        
        if let userName = userNameTextField.text, !userName.isEmpty {
            user.name = userName
        }
        user.about = userBioTextField.text.isEmpty ? nil : userBioTextField.text
        user.avatar = userAvatarImage.image ?? user.avatar
        
        savedConfiguration = user
        return user
    }
    
    private func updateUserInfo() {
        userNameTextField.text = user?.name
        userBioTextField.text = user?.about
        userAvatarImage.image = user?.avatar
    }
    
    private func updateButtonsState() {
        let isButtonEnabled =
            !activityIndicator.isAnimating &&
            (changedProperties[.name] ?? false ||
            changedProperties[.about] ?? false ||
            changedProperties[.avatar] ?? false)
        
        saveViaCoreDataButton.isEnabled = isButtonEnabled
    }
    
    // MARK: - Keyboard features
    private func setupHideKeyboardOnBackgroundTapGesture() {
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
    }
    
    @objc private func hideKeyboard() {
        userNameTextField.becomeFirstResponder()
        userNameTextField.resignFirstResponder()
    }
}


// MARK: - UITextViewDelegate (About)
extension EditProfileViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text != (user?.about ?? "") {
            changedProperties[.about] = true
        } else {
            changedProperties[.about] = false
        }
    }
}


// MARK: - UITextFieldDelegate (Name)
extension EditProfileViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
    // почему для изменения текста не придумали метод в делегате, и приходится через IB цеплять? :(
    @IBAction func nameTextFieldChanged(_ sender: UITextField) {
        if sender.text != user?.name {
            changedProperties[.name] = true
        } else {
            changedProperties[.name] = false
        }
    }
}


// MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        let avatar = info[UIImagePickerControllerEditedImage] as? UIImage
        self.handleUserAvatarChoose(avatar: avatar)
    }
    
    func showUserAvatarChooseActionSheet() {
        let alert = UIAlertController(title: "Выбери изображение профиля", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: self.chooseUserAvatarFromCamera))
        alert.addAction(UIAlertAction(title: "Выбрать из галереи", style: .default, handler: self.chooseUserAvatarFromGallery))
        alert.addAction(UIAlertAction(title: "Загрузить из интернета", style: .default, handler: self.chooseUserAvatarFromWeb))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func chooseUserAvatarFromCamera(_ action: UIAlertAction) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            self.showAlert(title: "Ошибка", message: "На устройстве нет камеры.")
            return
        }
        
        AVCaptureDevice.requestAccess(for: .video) { (granted) in
            if (!granted) {
                self.showAlert(title: "Ошибка", message: "Нет доступа к камере.")
                return
            }
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func chooseUserAvatarFromGallery(_ action: UIAlertAction) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            self.showAlert(title: "Ошибка", message: "Нет доступа к фотографиям.")
            return
        }
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func chooseUserAvatarFromWeb(_ action: UIAlertAction) {
        let controller = presentationAssembly.chooseAvatarProfileViewController()
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
}

// MARK: - ChooseAvatarProfileViewDelegate
extension EditProfileViewController: ChooseAvatarProfileViewDelegate {
    func avatarDidChange(image: UIImage) {
        self.handleUserAvatarChoose(avatar: image)
    }
}
