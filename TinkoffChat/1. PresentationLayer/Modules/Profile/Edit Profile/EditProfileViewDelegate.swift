//
//  EditProfileViewDelegate.swift
//  TinkoffChat
//
//  Created by bf on 21/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol EditProfileViewDelegate: class {
    func profileUpdated(user: IUser)
}
