//
//  ProfileViewController
//  TinkoffChat
//
//  Created by bf on 20/09/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit
import AVFoundation

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    // Dependencies
    private let presentationAssembly: IPresentationAssembly
    private let userService: IUserService
    
    // MARK: - UI components
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userBioLabel: UILabel!
    
    // MARK: - Data Source
    public var user: IUser?
    
    // MARK: - Lifecycle
    init(presentationAssembly: IPresentationAssembly, userService: IUserService) {
        self.presentationAssembly = presentationAssembly
        self.userService = userService
        super.init(nibName: "ProfileViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInfo()
    }
    
    
    // MARK: - Private methods
    private func loadInfo() {
        activityIndicator.startAnimating()
        userService.getCurrentUser(callback: handleDataManagerResponse)
    }
    
    private func handleDataManagerResponse(error: Bool, user: IUser?) {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicator.stopAnimating()
            
            if error {
                self?.showError(message: "Ошибка при загрузке данных профиля")
                return
            }
            
            self?.user = user
            self?.updateInfo()
        }
    }
    
    private func updateInfo() {
        self.userNameLabel.text = user?.name
        
        if user?.about?.isEmpty ?? true {
            self.userBioLabel.text = "Информация о себе не указана"
            self.userBioLabel.textColor = .lightGray
        } else {
            self.userBioLabel.text = user?.about
            self.userBioLabel.textColor = .darkGray
        }
        
        self.userAvatarImage.image = user?.avatar
        self.editProfileButton.isEnabled = true
        self.editProfileButton.borderColor = .black
    }
    
    private func showError(message: String) {
        let alert = UIAlertController(title: "Произошла ошибка", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Close button
    @IBAction func closeButtonTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Edit profile button
    @IBAction func editProfileButtonTap(_ sender: Any) {
        let vc = presentationAssembly.editProfileViewController()
        vc.user = user
        vc.delegate = self
        self.present(vc, animated: true)
    }
}


// MARK: - EditProfileViewDelegate
extension ProfileViewController: EditProfileViewDelegate {
    func profileUpdated(user: IUser) {
        // Но по заданию нужно выполнить чтение из файла
        loadInfo()
    }
}
