//
//  TinkoffGestureRecognizer.swift
//  TinkoffChat
//
//  Created by bf on 01/12/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit
import UIKit.UIGestureRecognizerSubclass

// Отвечает за анимацю логотипа под пальцем при любом касании
class TinkoffGestureRecognizer: UIGestureRecognizer {
    // MARK: - Properties
    private var particleImage: CGImage?
    private var emitter: CAEmitterLayer?

    // MARK: - Lifecycle
    override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
        
        self.particleImage = UIImage(named: "TinkoffParticle")?.cgImage
        self.isEnabled = true
        self.cancelsTouchesInView = false
        self.delegate = self
    }
    
    // MARK: - Protocol implementation
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        
        // если прикладывается >1 пальца, отключаем эффект
        if self.emitter != nil || touches.count != 1 {
            deleteEmitter()
            self.state = .failed
            return
        }
        
        guard let touch = touches.first else { return }
        let point = touch.location(in: self.view?.window)

        let emitter = createEmitter(in: point)
        self.emitter = emitter
        self.view?.layer.addSublayer(emitter)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        if touches.count != 1 { return }
        
        guard let touch = touches.first else { return }
        let point = touch.location(in: self.view?.window)
        
        moveEmitter(to: point)
        self.state = .changed
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        
        deleteEmitter()
        self.state = .recognized
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesCancelled(touches, with: event)
        
        deleteEmitter()
        self.state = .failed
    }
    
    // MARK: - Private methods
    private func createEmitter(in point: CGPoint) -> CAEmitterLayer {
        let emitter = CAEmitterLayer()
        emitter.emitterPosition = point
        emitter.emitterMode = kCAEmitterLayerOutline
        emitter.emitterShape = kCAEmitterLayerCircle
        emitter.renderMode = kCAEmitterLayerAdditive
        emitter.emitterSize = CGSize(width: 10, height: 0)
        
        let particle = CAEmitterCell()
        particle.birthRate = 40
        particle.lifetime = 0.7
        particle.lifetimeRange = 0.2
        particle.velocity = 120
        particle.velocityRange = 100
        particle.emissionRange = CGFloat.pi * 2.0
        particle.scale = 0.65
        particle.scaleSpeed = 0.3
        particle.contents = particleImage
        // эффект затухания
        // Для свойства color у CAEmitterCell, задаем alpha по формуле lifetime * -alphaSpeed,
        // где alphaSpeed = -1.0 / длительность_затухания
        particle.alphaSpeed = -1.0 / particle.lifetime
        let color = UIColor(red: 1, green: 1, blue: 1, alpha: CGFloat(particle.lifetime * -particle.alphaSpeed))
        particle.color = color.cgColor

        emitter.emitterCells = [particle]
        return emitter
    }
    
    private func moveEmitter(to point: CGPoint) {
        // CATransaction с ключом kCATransactionDisableActions отключает анимацию при перемещении
        // За счет этого получается след из частиц, который тянется за пальцем
        CATransaction.begin()
        CATransaction.setValue(true, forKey: kCATransactionDisableActions)
        self.emitter?.emitterPosition = point
        CATransaction.commit()
    }
    
    private func deleteEmitter() {
        self.emitter?.removeFromSuperlayer()
        self.emitter = nil
    }
}

// MARK: - UIGestureRecognizerDelegate
extension TinkoffGestureRecognizer: UIGestureRecognizerDelegate {
    // Нужно чтобы не конфликтовать с другими UIGestureRecognizer (в т.ч. системными)
    // Без этого текущий жест не будет работать, например, при прокрутке любых Scroll View
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
