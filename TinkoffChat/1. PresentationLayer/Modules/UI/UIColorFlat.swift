//
//  UIColorFlat.swift
//  TinkoffChat
//
//  Created by bf on 04/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit
import GameKit

extension UIColor {
    struct Flat { // цвета из iOS Flat UI Colors Kit: http://0xrgb.com/#ios
        static let darkBlue = UIColor(red:0.35, green:0.34, blue:0.84, alpha:1.0)
        static let deepBlue = UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0)
        static let blue = UIColor(red:0.20, green:0.67, blue:0.86, alpha:1.0)
        static let lightBlue = UIColor(red:0.35, green:0.78, blue:0.98, alpha:1.0)
        static let green = UIColor(red:0.30, green:0.85, blue:0.39, alpha:1.0)
        static let pink = UIColor(red:1.00, green:0.18, blue:0.33, alpha:1.0)
        static let red = UIColor(red:1.00, green:0.23, blue:0.19, alpha:1.0)
        static let orange = UIColor(red:1.00, green:0.58, blue:0.00, alpha:1.0)
        static let yellow = UIColor(red:1.00, green:0.80, blue:0.00, alpha:1.0)
        static let gray = UIColor(red:0.56, green:0.56, blue:0.58, alpha:1.0)
        
        // возвращает рандомный Flat UI цвет
        // при вызове с одинаковыми значениями seed, будет сгенерирован один и тот же цвет
        static func random(seed: Int?) -> UIColor {
            return [UIColor.Flat.darkBlue, UIColor.Flat.deepBlue, UIColor.Flat.blue, UIColor.Flat.lightBlue, UIColor.Flat.green, UIColor.Flat.pink, UIColor.Flat.red, UIColor.Flat.orange, UIColor.Flat.yellow, UIColor.Flat.gray].random(seed: seed) ?? UIColor.Flat.gray
        }
    }
}
