//
//  UITextViewField.swift
//  TinkoffChat
//
//  Created by bf on 20/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit

@IBDesignable
class UITextViewField: UITextView {
    fileprivate let textFieldBorderColor = UIColor.gray.withAlphaComponent(0.5).cgColor

    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyles()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        applyStyles()
    }

    fileprivate func applyStyles() {
        self.layer.cornerRadius = 5
        self.layer.borderColor = textFieldBorderColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
    }
}
