//
//  ServicesAssembly.swift
//  TinkoffChat
//
//  Created by bf on 17/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IServicesAssembly {
    var userService: IUserService { get }
    var conversationService: IConversationService { get }
    var communcationService: ICommunicationService { get }
    var messageService: IMessageService { get }
    var imageSearchService: IImageSearchService { get }
}

class ServicesAssembly: IServicesAssembly {
    private let coreAssembly: ICoreAssembly
    
    init(coreAssembly: ICoreAssembly) {
        self.coreAssembly = coreAssembly
    }
    
    lazy var userService: IUserService =
        UserService(userStorage: self.coreAssembly.userStorage,
                    currentUserDataManager: self.coreAssembly.currentUserDataManager)
    
    lazy var conversationService: IConversationService =
        ConversationService(conversationStorage: self.coreAssembly.conversationStorage)
    
    lazy var messageService: IMessageService =
        MessageService(messageStorage: self.coreAssembly.messageStorage)
    
    lazy var communcationService: ICommunicationService =
        CommunicationService(communicationLayer: self.coreAssembly.communicationLayer,
                             userStorage: self.coreAssembly.userStorage,
                             conversationStorage: self.coreAssembly.conversationStorage)
    
    lazy var imageSearchService: IImageSearchService =
        PixabayImageSearchService(networkLayer: self.coreAssembly.networkLayer,
                                  imageDownloader: self.coreAssembly.imageDownloader,
                                  imageCache: self.coreAssembly.imageCache)
}
