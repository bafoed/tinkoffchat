//
//  ICommunicationService.swift
//  TinkoffChat
//
//  Created by bf on 19/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol ICommunicationService: CommunicationLayerDelegate {
    func start()
    func stop()
    
    func send(_ message: IMessage, to user: IUser)
    func isOnline(user: IUser) -> Bool
}

class CommunicationService: ICommunicationService {
    // MARK: - Dependencies
    private let communicationLayer: ICommunicationLayer
    private let userStorage: IUserStorage
    private let conversationStorage: IConversationStorage
    
    init(communicationLayer: ICommunicationLayer, userStorage: IUserStorage, conversationStorage: IConversationStorage) {
        self.communicationLayer = communicationLayer
        self.userStorage = userStorage
        self.conversationStorage = conversationStorage
    }
    
    func start() {
        communicationLayer.online = true
        communicationLayer.delegate = self
    }
    
    func stop() {
        communicationLayer.online = false
        communicationLayer.delegate = nil
    }
    
    func send(_ message: IMessage, to user: IUser) {
        communicationLayer.send(message, to: userToPeer(user))
    }
    
    func isOnline(user: IUser) -> Bool {
        return communicationLayer.isOnline(peer: userToPeer(user))
    }
    
    private func userToPeer(_ user: IUser) -> Peer {
        return Peer(identifier: user.identifier, name: user.name)
    }
}

extension CommunicationService: CommunicationLayerDelegate {
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didFoundPeer peer: Peer) {
        if var user = userStorage.get(with: peer.identifier) {
            guard var conversation = conversationStorage.get(for: user) else { return }
            user.name = peer.name
            conversation.online = true
            
            let _ = userStorage.createOrUpdate(model: user)
            let _ = conversationStorage.createOrUpdate(model: conversation, user: user)
        } else {
            let user = User(identifier: peer.identifier,
                             name: peer.name,
                             about: nil,
                             avatar: nil)
            
            guard let _ = userStorage.createOrUpdate(model: user) else { return }
            
            let conversation = Conversation(identifier: Conversation.generateConversationId(),
                                            online: true,
                                            hasUnreadMessages: false)
            
            let _ = conversationStorage.createOrUpdate(model: conversation, user: user)
        }
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didLostPeer peer: Peer) {
        guard let user = userStorage.get(with: peer.identifier) else { return }
        guard var conversation = conversationStorage.get(for: user) else { return }
        conversation.online = false
        let _ = conversationStorage.createOrUpdate(model: conversation, user: user)
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didNotStartBrowsingForPeers error: Error) {
        print("Error")
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didReceiveInviteFromPeer peer: Peer, invintationClosure: (Bool) -> Void) {
        invintationClosure(true)
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didNotStartAdvertisingForPeers error: Error) {
        print("Error")
    }
    
    func communicationLayer(_ communicationLayer: ICommunicationLayer, didReceiveMessage message: Message, from peer: Peer) {
        guard let user = userStorage.get(with: peer.identifier) else { return }
        guard let conversation = conversationStorage.get(for: user) else { return }
        
        conversationStorage.updateReadStatus(conversation: conversation, read: false)
        conversationStorage.addMessage(message: message, to: conversation)
    }
    
    
}
