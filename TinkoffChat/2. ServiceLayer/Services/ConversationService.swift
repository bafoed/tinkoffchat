//
//  ConversationService.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IConversationServiceDelegate: IConversationStorageTableViewDelegate {}
protocol IConversationServiceOnlineDelegate: IConversationStorageConversationOnlineDelegate {}

protocol IConversationService {
    // MARK: - Online conversations
    func onlineConversationsNumberOfSections() -> Int
    func onlineConversationsNumberOfRowsInSection(section: Int) -> Int
    func onlineConversationsObject(at indexPath: IndexPath) -> IConversation?
    var onlineConversationsDelegate: IConversationServiceDelegate? { get set }

    // MARK: - Conversation
    func getConversationWithIdentifier(_ identifier: String) -> IConversation?
    func updateConversationReadStatus(conversation: IConversation, read: Bool)

    // MARK: - Conversation online status
    func setConversationForOnlineDelegate(conversation: IConversation)
    var onlineDelegate: IConversationServiceOnlineDelegate? { get set }
    
    // MARK: - Messages
    func addMessage(message: IMessage, to conversation: IConversation)
    func getLatestMessage(in conversation: IConversation) -> IMessage?
    
    // MARK: - User
    func getUser(in conversation: IConversation) -> IUser?
}

class ConversationService: IConversationService {
    // Dependencies
    private var conversationStorage: IConversationStorage
    
    init(conversationStorage: IConversationStorage) {
        self.conversationStorage = conversationStorage
    }
    
    func getConversationWithIdentifier(_ identifier: String) -> IConversation? {
        return conversationStorage.get(with: identifier)
    }
    
    func addMessage(message: IMessage, to conversation: IConversation) {
        conversationStorage.addMessage(message: message, to: conversation)
    }
    
    func getLatestMessage(in conversation: IConversation) -> IMessage? {
        return conversationStorage.getLatestMessage(in: conversation)
    }
    
    func getUser(in conversation: IConversation) -> IUser? {
        return conversationStorage.getUser(in: conversation)
    }
    
    func updateConversationReadStatus(conversation: IConversation, read: Bool) {
        conversationStorage.updateReadStatus(conversation: conversation, read: read)
    }
    
    weak var onlineConversationsDelegate: IConversationServiceDelegate? {
        didSet {
            conversationStorage.delegate = onlineConversationsDelegate
        }
    }
    
    func onlineConversationsNumberOfSections() -> Int {
        return conversationStorage.numberOfSections()
    }
    
    func onlineConversationsNumberOfRowsInSection(section: Int) -> Int {
        return conversationStorage.numberOfRowsInSection(section: section)
    }
    
    func onlineConversationsObject(at indexPath: IndexPath) -> IConversation? {
        return conversationStorage.object(at: indexPath)
    }

    func setConversationForOnlineDelegate(conversation: IConversation) {
        conversationStorage.setConversationForOnlineDelegate(conversation: conversation)
    }
    
    weak var onlineDelegate: IConversationServiceOnlineDelegate? {
        didSet {
            conversationStorage.onlineDelegate = onlineDelegate
        }
    }

}
