//
//  IImageSearchService.swift
//  TinkoffChat
//
//  Created by bf on 30/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IImageSearchService {
    func search(with query: String,
                onSuccess: @escaping ([IImageSearchResult]) -> Void,
                onError: @escaping (Error) -> Void
    )
    
    func download(image: IRemoteImage,
                  onSuccess: @escaping (IDownloadedImage) -> Void,
                  onError: @escaping (Error) -> Void) -> INetworkLayerTask?
}
