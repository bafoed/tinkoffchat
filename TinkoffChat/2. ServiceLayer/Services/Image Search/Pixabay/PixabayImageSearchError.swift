//
//  PixabayImageSearchError.swift
//  TinkoffChat
//
//  Created by bf on 30/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

enum PixabayImageSearchError: LocalizedError {
    case characters
    case apiUrl
    case decodeFailed
    
    public var localizedDescription: String {
        switch self {
        case .characters:
            return "Invalid characters in query"
        case .apiUrl:
            return "Invalid API URL"
        case .decodeFailed:
            return "Failed to decode response"
        }
    }
}
