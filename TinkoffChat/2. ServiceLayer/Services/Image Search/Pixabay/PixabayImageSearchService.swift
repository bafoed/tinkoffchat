//
//  PixabayImageSearchService.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

class PixabayImageSearchService: IImageSearchService {
    // Properties
    static let apiKey = "10771764-f2e68b3bc5d498e0eaf4fad32"
    static let apiURL = "https://pixabay.com/api/?key=\(PixabayImageSearchService.apiKey)&image_type=photo&pretty=false&per_page=200&q=%@"

    // Dependencies
    private let networkLayer: INetworkLayer
    private let imageDownloader: IImageDownloader
    private let imageCache: IImageCache
    
    init(networkLayer: INetworkLayer, imageDownloader: IImageDownloader, imageCache: IImageCache) {
        self.networkLayer = networkLayer
        self.imageDownloader = imageDownloader
        self.imageCache = imageCache
    }
    
    // MARK: - Protocol implementation
    func search(with query: String,
                onSuccess: @escaping ([IImageSearchResult]) -> Void,
                onError: @escaping (Error) -> Void) {
        
        guard let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            onError(PixabayImageSearchError.characters)
            return
        }
        
        guard let url = URL(string: String(format: PixabayImageSearchService.apiURL, encodedQuery)) else {
            onError(PixabayImageSearchError.apiUrl)
            return
        }
        
        // используем вариант с generic, а не с Data
        let successHandler: (PixabayResponse) -> Void = { (response) in
            guard let result = self.map(response: response) else {
                onError(PixabayImageSearchError.decodeFailed)
                return
            }
            onSuccess(result)
        }
        
        let _ = networkLayer.get(url: url,
                                 headers: [:],
                                 onSuccess: successHandler,
                                 onError: onError)
    }
    
    func download(image: IRemoteImage,
                  onSuccess: @escaping (IDownloadedImage) -> Void,
                  onError: @escaping (Error) -> Void) -> INetworkLayerTask? {
        
        if imageCache.contains(image), let downloadedImage = imageCache.get(image) {
            onSuccess(downloadedImage)
            return nil
        }
        
        let successHandler: (IDownloadedImage) -> Void = { (downloadedImage) in
            self.imageCache.set(image, downloadedImage)
            onSuccess(downloadedImage)
        }
        
        return imageDownloader.download(image,
                                        onSuccess: successHandler,
                                        onError: onError)
    }
    
    // MARK: - Private methods
    private func map(response: PixabayResponse) -> [IImageSearchResult]? {
        guard let hits = response.hits else { return nil }
        
        var result = [IImageSearchResult]()
        for hit in hits {
            guard
                let previewURL = hit.previewURL,
                let fullURL = hit.webformatURL
                else { continue }
            
            let previewRemoteImage = RemoteImage(imageURL: previewURL)
            let fullRemoteImage = RemoteImage(imageURL: fullURL)
            result.append(ImageSearchResult(preview: previewRemoteImage, full: fullRemoteImage))
        }
        
        return result
    }
}
