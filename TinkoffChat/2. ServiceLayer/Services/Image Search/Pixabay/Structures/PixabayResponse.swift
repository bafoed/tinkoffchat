//
//  PixabayResponse.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

struct PixabayResponse: Codable {
    let totalHits: Int?
    let hits: [PixabayHit]?
}
