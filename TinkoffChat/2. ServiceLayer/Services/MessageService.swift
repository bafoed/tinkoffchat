//
//  MessageService.swift
//  TinkoffChat
//
//  Created by bf on 19/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IMessageServiceDelegate: IMessageStorageTableViewDelegate {}

protocol IMessageService {
    // MARK: - Current conversation table
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func object(at indexPath: IndexPath) -> IMessage?
    var delegate: IMessageServiceDelegate? { get set }
    func setConversationForTableView(conversation: IConversation)
}

class MessageService: IMessageService {
    // MARK: - Dependencies
    private var messageStorage: IMessageStorage
    
    init(messageStorage: IMessageStorage) {
        self.messageStorage = messageStorage
    }
    
    func numberOfSections() -> Int {
        return messageStorage.numberOfSections()
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return messageStorage.numberOfRowsInSection(section: section)
    }
    
    func object(at indexPath: IndexPath) -> IMessage? {
        return messageStorage.object(at: indexPath)
    }
    
    func setConversationForTableView(conversation: IConversation) {
        messageStorage.setConversationForTableView(conversation: conversation)
    }
    
    weak var delegate: IMessageServiceDelegate? {
        didSet {
            self.messageStorage.delegate = delegate
        }
    }
}
