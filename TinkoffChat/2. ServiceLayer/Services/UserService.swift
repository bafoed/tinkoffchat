//
//  UserService.swift
//  TinkoffChat
//
//  Created by bf on 21/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IUserService {
    func getUserWithIdentifier(_ identifier: String) -> IUser?

    func getCurrentUser(callback: @escaping (Bool, IUser?) -> Void)
    func updateCurrentUser(currentUser: IUser, callback: @escaping (Bool) -> Void)
}

class UserService: IUserService {
    // MARK: - Dependencies
    private let userStorage: IUserStorage
    private let currentUserDataManager: ICurrentUserDataManager

    init(userStorage: IUserStorage, currentUserDataManager: ICurrentUserDataManager) {
        self.userStorage = userStorage
        self.currentUserDataManager = currentUserDataManager
    }
    
    // MARK: - Implementation
    func getUserWithIdentifier(_ identifier: String) -> IUser? {
        return userStorage.get(with: identifier)
    }
    
    func getCurrentUser(callback: @escaping (Bool, IUser?) -> Void) {
        currentUserDataManager.get(callback: callback)
    }
    
    func updateCurrentUser(currentUser: IUser, callback: @escaping (Bool) -> Void) {
        currentUserDataManager.update(configuration: currentUser, callback: callback)
    }
}
