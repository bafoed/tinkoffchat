//
//  CoreAssembly.swift
//  TinkoffChat
//
//  Created by bf on 17/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol ICoreAssembly {
    var conversationStorage: IConversationStorage { get }
    var userStorage: IUserStorage { get }
    var currentUserDataManager: ICurrentUserDataManager { get }
    var messageStorage: IMessageStorage { get }
    var communicationLayer: ICommunicationLayer { get }
    var networkLayer: INetworkLayer { get }
    var imageDownloader: IImageDownloader { get }
    var imageCache: IImageCache { get }
}

class CoreAssembly: ICoreAssembly {
    
    lazy var coreDataStack = CoreDataStack()
    lazy var coreDataMapper = CoreDataMapper()
    
    private lazy var coreDataUserStorage: CoreDataUserStorage =
        CoreDataUserStorage(coreDataStack: self.coreDataStack,
                            coreDataMapper: self.coreDataMapper)
    
    private lazy var coreDataMessageStorage: CoreDataMessageStorage =
        CoreDataMessageStorage(coreDataStack: self.coreDataStack,
                               coreDataMapper: self.coreDataMapper)

    lazy var conversationStorage: IConversationStorage =
        CoreDataConversationStorage(coreDataStack: self.coreDataStack,
                                    coreDataMapper: self.coreDataMapper,
                                    coreDataUserStorage: self.coreDataUserStorage,
                                    coreDataMessageStorage: self.coreDataMessageStorage)
    
    lazy var userStorage: IUserStorage = self.coreDataUserStorage
    
    lazy var messageStorage: IMessageStorage = self.coreDataMessageStorage

    lazy var currentUserDataManager: ICurrentUserDataManager =
        CoreDataCurrentUserDataManager(userStorage: self.userStorage)
    
    lazy var communicationLayer: ICommunicationLayer =
        MPCCommunicationLayer(currentUserDataManager: self.currentUserDataManager)
    
    lazy var networkLayer: INetworkLayer = URLSessionNetworkLayer()
    
    lazy var imageDownloader: IImageDownloader = ImageDownloader(networkLayer: self.networkLayer)
    
    lazy var imageCache: IImageCache = MemoryImageCache()
}
