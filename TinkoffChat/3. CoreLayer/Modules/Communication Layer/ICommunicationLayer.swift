//
//  ICommunicationLayer.swift
//  TinkoffChat
//
//  Created by bf on 28/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol ICommunicationLayer: class {
    var delegate: CommunicationLayerDelegate? { get set }
    var online: Bool { get set }
    func send(_ message: IMessage, to peer: Peer)
    func isOnline(peer: Peer) -> Bool
}

protocol CommunicationLayerDelegate: class {
    /// Browsing
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didFoundPeer peer: Peer)
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didLostPeer peer: Peer)
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didNotStartBrowsingForPeers error: Error)
    /// Advertising
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didReceiveInviteFromPeer peer: Peer,
                              invintationClosure: (Bool) -> Void)
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didNotStartAdvertisingForPeers error: Error)
    /// Messages
    func communicationLayer(_ communicationLayer: ICommunicationLayer,
                              didReceiveMessage message: Message,
                              from peer: Peer)
}


