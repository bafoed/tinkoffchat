//
//  MPCCommunicationLayer.swift
//  TinkoffChat
//
//  Created by bf on 28/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class MPCCommunicationLayer: NSObject, ICommunicationLayer {
    let chatServiceType = "tinkoff-chat"
    
    weak var delegate: CommunicationLayerDelegate?
    var online: Bool = false {
        didSet {
            if online && ready {
                serviceBrowser.startBrowsingForPeers()
                serviceAdvertiser.startAdvertisingPeer()
            } else {
                serviceBrowser.stopBrowsingForPeers()
                serviceAdvertiser.stopAdvertisingPeer()
            }
        }
    }
    
    private var peerId: MCPeerID
    private var session: MCSession
    private var serviceBrowser: MCNearbyServiceBrowser
    private var serviceAdvertiser: MCNearbyServiceAdvertiser
    private var foundPeers = [MCPeerID: Peer]()
    
    private var ready = false
    private let currentUserDataManager: ICurrentUserDataManager
    
    init(currentUserDataManager: ICurrentUserDataManager) {
        self.currentUserDataManager = currentUserDataManager
        
        let initialDisplayName = UIDevice.current.identifierForVendor?.uuidString ?? "Unknown device"
        peerId = MCPeerID(displayName: initialDisplayName)
        session = MCSession(peer: peerId)
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: peerId, discoveryInfo: ["userName": initialDisplayName], serviceType: chatServiceType)
        serviceBrowser = MCNearbyServiceBrowser(peer: peerId, serviceType: chatServiceType)

        super.init()
        
        session.delegate = self
        serviceBrowser.delegate = self
        serviceAdvertiser.delegate = self
        
        initForCurrentUser()
    }
    
    func initForCurrentUser() {
        currentUserDataManager.get { error, user in
            self.online = false
            guard var user = user else { return }
            if user.name == User.createDefaultUser().name {
                user.name += " \(arc4random() % 100)"
            }
            self.peerId = MCPeerID(displayName: user.name)
            self.session = MCSession(peer: self.peerId)
            self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: self.peerId, discoveryInfo: ["userName": user.name], serviceType: self.chatServiceType)
            self.serviceBrowser = MCNearbyServiceBrowser(peer: self.peerId, serviceType: self.chatServiceType)
            
            self.session.delegate = self
            self.serviceBrowser.delegate = self
            self.serviceAdvertiser.delegate = self
            self.ready = true
            self.online = true
        }
    }
    
    func send(_ message: IMessage, to peer: Peer) {
        if let peerId = foundPeers.filter({ return $1 == peer }).first?.key {
            do {
                try session.send(message.encodeToJSON()!, toPeers: [peerId], with: .reliable)
            } catch {
                print("failed", error)
            }
        }
    }
    
    func lookForPeer(peer: Peer) -> MCPeerID? {
        if let result = foundPeers.first(where: { $1 == peer }) {
            return result.key
        } else {
            return nil
        }
    }
    
    func isOnline(peer: Peer) -> Bool {
        guard let peerID = lookForPeer(peer: peer) else { return false }
        return foundPeers[peerID] != nil
    }
}

extension MPCCommunicationLayer: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        guard let peer = foundPeers[peerID] else { return }
    
        if state == .connected {
            self.delegate?.communicationLayer(self, didFoundPeer: peer)
        }
        
        if state == .notConnected {
            let peer = self.foundPeers[peerID]!
            self.foundPeers.removeValue(forKey: peerID)
            self.delegate?.communicationLayer(self, didLostPeer: peer)
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        guard
            let peer = foundPeers[peerID],
            let messageJSON = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
            let type = MessageType.init(rawValue: messageJSON?["eventType"] as! String),
            let identifier = messageJSON?["messageId"] as? String,
            let messageText = messageJSON?["text"] as? String
        else {
            return
        }
    
        let message = Message(identifier: identifier, text: messageText, direction: .received, type: type, date: Date())
        delegate?.communicationLayer(self, didReceiveMessage: message, from: peer)
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }
}

extension MPCCommunicationLayer: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        delegate?.communicationLayer(self, didNotStartAdvertisingForPeers: error)
    }
    
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        if let peer = foundPeers[peerID] {
            delegate?.communicationLayer(self, didReceiveInviteFromPeer: peer, invintationClosure: { result in
                invitationHandler(result, self.session)
            })
        }
    }
}

extension MPCCommunicationLayer: MCNearbyServiceBrowserDelegate {
    func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        delegate?.communicationLayer(self, didNotStartBrowsingForPeers: error)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
        guard let userName = info?["userName"] else { return }
        let peer = Peer(identifier: userName, name: peerID.displayName)
        
        if let index = foundPeers.index(where: { $1 == peer }) {
            self.foundPeers.remove(at: index)
        }
        
        self.foundPeers[peerID] = peer
        self.delegate?.communicationLayer(self, didFoundPeer: peer)
        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
    }
    
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        if self.foundPeers[peerID] != nil {
            let peer = self.foundPeers[peerID]!
            self.foundPeers.removeValue(forKey: peerID)
            self.delegate?.communicationLayer(self, didLostPeer: peer)
        }
    }
}
