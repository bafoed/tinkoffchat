//
//  CoreDataCurrentUserDataManager.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

class CoreDataCurrentUserDataManager: ICurrentUserDataManager {
    // MARK: - Dependencies
    private let userStorage: IUserStorage
    
    // MARK: - Private properties
    private let ownIdentifier = "me"
    private let gcdQueue = DispatchQueue(label: "ru.bf.TinkoffChat.CoreDataUserInfoDataManager-queue")

    init(userStorage: IUserStorage) {
        self.userStorage = userStorage
    }
    
    func get(callback: @escaping (Bool, IUser?) -> Void) {
        gcdQueue.async {
            let user = self.userStorage.get(with: self.ownIdentifier) ?? User.createDefaultUser()
            callback(false, user)
        }
    }
    
    func update(configuration: IUser, callback: @escaping (Bool) -> Void) {
        gcdQueue.async {
            var user = configuration
            user.identifier = self.ownIdentifier
            if let _ = self.userStorage.createOrUpdate(model: user) {
                callback(false)
            } else {
                callback(true)
            }
        }
    }
}
