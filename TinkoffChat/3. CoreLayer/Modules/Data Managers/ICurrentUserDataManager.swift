//
//  ICurrentUserDataManager.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol ICurrentUserDataManager {
    func get(callback: @escaping (Bool, IUser?) -> Void)
    func update(configuration: IUser, callback: @escaping (Bool) -> Void)
}

