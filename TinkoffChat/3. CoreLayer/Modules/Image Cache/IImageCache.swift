//
//  IImageCache.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

// URLSession сам кеширует ответы, но сетевой слой может быть и не на URLSession, кешировать картинки не всегда нужно только в памяти, а некоторые бэкенды могут неправильно отдавать заголовки кеширования...
protocol IImageCache {
    func contains(_ remoteImage: IRemoteImage) -> Bool
    
    func get(_ remoteImage: IRemoteImage) -> IDownloadedImage?
    func set(_ remoteImage: IRemoteImage, _ downloadedImage: IDownloadedImage)
}
