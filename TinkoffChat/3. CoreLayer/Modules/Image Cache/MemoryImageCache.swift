//
//  MemoryImageCache.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

class MemoryImageCache: IImageCache {
    // MARK: - Properties
    private var cache: [URL: IDownloadedImage] = [:]
    private let queue = DispatchQueue(label: "ru.bf.TinkoffChat.MemoryImageCache-queue")
    
    // MARK: - Protocol implementation
    func contains(_ remoteImage: IRemoteImage) -> Bool {
        return cache[remoteImage.imageURL] != nil
    }
    
    func get(_ remoteImage: IRemoteImage) -> IDownloadedImage? {
        return cache[remoteImage.imageURL]
    }
    
    func set(_ remoteImage: IRemoteImage, _ downloadedImage: IDownloadedImage) {
        // потокобезопасная запись в кеш
        queue.sync {
            cache[remoteImage.imageURL] = downloadedImage
        }
    }
}
