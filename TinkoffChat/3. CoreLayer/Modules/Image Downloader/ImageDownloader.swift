//
//  IImageDownloader.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

enum ImageDownloaderError: LocalizedError {
    case imageDecodeFailed
    
    public var localizedDescription: String {
        switch self {
        case .imageDecodeFailed:
            return "Can't decode image"
        }
    }
}

protocol IImageDownloader {
    func download(_ image: IRemoteImage,
                  onSuccess: @escaping (IDownloadedImage) -> Void,
                  onError: @escaping (Error) -> Void) -> INetworkLayerTask?
}

class ImageDownloader: IImageDownloader {
    // Dependencies
    private let networkLayer: INetworkLayer
    
    init(networkLayer: INetworkLayer) {
        self.networkLayer = networkLayer
    }
    
    // MARK: - Protocol implementation
    func download(_ image: IRemoteImage,
                  onSuccess: @escaping (IDownloadedImage) -> Void,
                  onError: @escaping (Error) -> Void) -> INetworkLayerTask? {
        
        let successHandler: (Data) -> Void = { data in
            guard let image = UIImage(data: data) else {
                onError(ImageDownloaderError.imageDecodeFailed)
                return
            }
            let downloadedImage = DownloadedImage(image: image)
            onSuccess(downloadedImage)
        }
        
        return networkLayer.get(url: image.imageURL,
                                headers: [:],
                                onSuccess: successHandler,
                                onError: onError)
    }

}
