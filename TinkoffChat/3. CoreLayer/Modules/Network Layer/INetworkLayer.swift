//
//  INetworkLayer.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation 

protocol INetworkLayerTask: class {
    func resume()
    func cancel()
}

protocol INetworkLayer: class {
    func get(url: URL,
             headers: [String: String],
             onSuccess: @escaping (_ data: Data) -> Void,
             onError: @escaping (_ error: Error) -> Void) -> INetworkLayerTask?
    
    func get<T: Decodable>(url: URL,
                           headers: [String: String],
                           onSuccess: @escaping (_ response: T) -> Void,
                           onError: @escaping (_ error: Error) -> Void) -> INetworkLayerTask?
}
