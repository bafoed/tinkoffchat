//
//  URLSessionNetworkLayer.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

enum URLSessionNetworkLayerError: LocalizedError {
    case generic
    case noResponse
    case serverError
    case decodingFailed
    
    var errorDescription: String? {
        switch self {
        case .generic:
            return "Generic error occured"
        case .noResponse:
            return "Can't get response from server"
        case .serverError:
            return "Error on server side"
        case .decodingFailed:
            return "Can't parse response to expected type"
        }
    }
}

class URLSessionNetworkLayer: INetworkLayer {
    // MARK: - Protocol implementation
    func get(url: URL,
             headers: [String : String],
             onSuccess: @escaping (Data) -> Void,
             onError: @escaping (Error) -> Void) -> INetworkLayerTask? {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error != nil  {
                onError(URLSessionNetworkLayerError.generic)
                return
            }
            
            guard let response = response, let data = data else {
                onError(URLSessionNetworkLayerError.noResponse)
                return
            }
            
            if !self.isSuccessCode(response) {
                onError(URLSessionNetworkLayerError.serverError)
                return
            }
            
            onSuccess(data)
        }
        
        let networkTask = URLSessionNetworkLayerTask(task: dataTask)
        networkTask.resume()
        
        return networkTask
    }
    
    func get<T: Decodable>(url: URL,
                headers: [String : String],
                onSuccess: @escaping (T) -> Void,
                onError: @escaping (Error) -> Void) -> INetworkLayerTask? {
        
        let successHandler: (Data) -> Void = { data in
            guard let responseObject = try? JSONDecoder().decode(T.self, from: data) else {
                onError(URLSessionNetworkLayerError.decodingFailed)
                return
            }
            
            onSuccess(responseObject)
        }
        
        return self.get(url: url,
                        headers: headers,
                        onSuccess: successHandler,
                        onError: onError)
    }


    // MARK: - Private methods
    private func isSuccessCode(_ statusCode: Int) -> Bool {
        return statusCode >= 200 && statusCode < 300
    }
    
    private func isSuccessCode(_ response: URLResponse?) -> Bool {
        guard let urlResponse = response as? HTTPURLResponse else {
            return false
        }
        return isSuccessCode(urlResponse.statusCode)
    }
}
