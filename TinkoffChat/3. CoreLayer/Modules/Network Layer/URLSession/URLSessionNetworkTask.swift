//
//  URLSessionNetworkTask.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

// простая обертка над URLSessionTask, в целях dependency inversion principle
class URLSessionNetworkLayerTask: INetworkLayerTask {
    // Properties
    private let task: URLSessionTask
    
    init(task: URLSessionTask) {
        self.task = task
    }
    
    // MARK: - Protocol implementation
    func resume() {
        self.task.resume()
    }
    
    func cancel() {
        self.task.cancel()
    }
}
