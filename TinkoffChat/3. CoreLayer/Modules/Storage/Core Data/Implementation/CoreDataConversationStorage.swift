//
//  CoreDataConversationStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

class CoreDataConversationStorage: NSObject, IConversationStorage, ICoreDataStorage {
    weak var delegate: IConversationStorageTableViewDelegate?
    weak var onlineDelegate: IConversationStorageConversationOnlineDelegate?

    typealias CoreDataModel = ConversationEntity
    
    var coreDataStack: ICoreDataStack
    var coreDataMapper: ICoreDataMapper
    var coreDataUserStorage: ICoreDataUserStorage
    var coreDataMessageStorage: ICoreDataMessageStorage

    fileprivate var onlineConversationsFetchedResultsController: NSFetchedResultsController<CoreDataModel>
    fileprivate var onlineStatusFetchedResultsController: NSFetchedResultsController<CoreDataModel>?
    
    init(coreDataStack: ICoreDataStack, coreDataMapper: ICoreDataMapper, coreDataUserStorage: ICoreDataUserStorage, coreDataMessageStorage: ICoreDataMessageStorage) {
        self.coreDataStack = coreDataStack
        self.coreDataMapper = coreDataMapper
        self.coreDataUserStorage = coreDataUserStorage
        self.coreDataMessageStorage = coreDataMessageStorage
        
        let onlineConversationsFetchRequest: NSFetchRequest<CoreDataModel> =
            coreDataStack.makeFetchRequest(
                NSPredicate(format: "online == true"),
                [NSSortDescriptor(key: #keyPath(CoreDataModel.hasUnreadMessages), ascending: false)]
            )
        
        self.onlineConversationsFetchedResultsController = coreDataStack.makeFetchedResultsController(onlineConversationsFetchRequest)

        super.init()

        self.onlineConversationsFetchedResultsController.delegate = self
        try? self.onlineConversationsFetchedResultsController.performFetch()
    }
    
    // MARK: - Protocol implementation
    func get(with identifier: String) -> IConversation? {
        guard let entity = getEntity(with: identifier) else {
            return nil
        }
        return coreDataMapper.map(entity: entity)
    }
    
    func get(for user: IUser) -> IConversation? {
        guard
            let userEntity = coreDataUserStorage.getEntity(with: user.identifier),
            let entity = userEntity.conversation
        else {
            return nil
        }
        return coreDataMapper.map(entity: entity)
    }
    
    func addMessage(message: IMessage, to conversation: IConversation) {
        guard let conversationEntity = getEntity(with: conversation.identifier) else { return }
        
        var messageEntity = coreDataMessageStorage.createEntity()
        coreDataMapper.map(model: message, entity: &messageEntity)
        messageEntity.conversation = conversationEntity
        
        let _ = try? coreDataStack.viewContext.save()
    }
    
    func getLatestMessage(in conversation: IConversation) -> IMessage? {
        guard let conversationEntity = getEntity(with: conversation.identifier) else { return nil }
        guard let latestMessage = conversationEntity.messages?.lastObject else { return nil }
        guard let message = latestMessage as? MessageEntity else { return nil }
        return coreDataMapper.map(entity: message)
    }
    
    func getUser(in conversation: IConversation) -> IUser? {
        guard let conversationEntity = getEntity(with: conversation.identifier) else { return nil }
        return coreDataMapper.map(entity: conversationEntity.user!)
    }

    func updateReadStatus(conversation: IConversation, read: Bool) {
        guard let conversationEntity = getEntity(with: conversation.identifier) else { return }
        conversationEntity.hasUnreadMessages = !read
        let _ = try? coreDataStack.viewContext.save()
    }

    func createOrUpdate(model: IConversation, user: IUser) -> IConversation? {
        var entity = getEntity(with: model.identifier) ?? createEntity()
        coreDataMapper.map(model: model, entity: &entity)
        
        guard let userEntity = coreDataUserStorage.getEntity(with: user.identifier) else {
            return nil
        }
        
        entity.user = userEntity
        
        guard let _ = try? coreDataStack.viewContext.save() else {
            return nil
        }
        
        return model
    }

    func numberOfSections() -> Int {
        return onlineConversationsFetchedResultsController.sections?.count ?? 0
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return onlineConversationsFetchedResultsController.sections?[section].numberOfObjects ?? 0
    }
    
    func object(at indexPath: IndexPath) -> IConversation? {
        return coreDataMapper.map(entity: onlineConversationsFetchedResultsController.object(at: indexPath))
    }
    
    func markAllOffline() {
        let fetchRequest: NSFetchRequest<CoreDataModel> = coreDataStack.makeFetchRequest(nil)
        if let result = try? coreDataStack.viewContext.fetch(fetchRequest) {
            for entity in result {
                entity.online = false
            }
            try? coreDataStack.viewContext.save()
        }
    }
    
    func setConversationForOnlineDelegate(conversation: IConversation) {
        onlineStatusFetchedResultsController = coreDataStack.makeFetchedResultsController(
            NSPredicate(format: "identifier == %@ AND online == true", conversation.identifier),
            [NSSortDescriptor(key: #keyPath(ConversationEntity.identifier), ascending: true)]
        )
        
        onlineStatusFetchedResultsController?.delegate = self
        try? onlineStatusFetchedResultsController?.performFetch()
    }

}

extension CoreDataConversationStorage: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == onlineConversationsFetchedResultsController {
            delegate?.willChangeContent()
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if controller == onlineConversationsFetchedResultsController {
            delegate?.contentChanged(didChange: anObject,
                                     at: indexPath,
                                     for: ConversationStorageTableViewChangeType(rawValue: type.rawValue)!,
                                     newIndexPath: newIndexPath)
        }
        
        if controller == onlineStatusFetchedResultsController {
            if type == .delete {
                onlineDelegate?.didBecomeOffline()
            } else if type == .insert {
                onlineDelegate?.didBecomeOnline()
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller == onlineConversationsFetchedResultsController {
            delegate?.didChangeContent()
        }
    }
}
