//
//  CoreDataMessageStorage.swift
//  structure
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

protocol ICoreDataMessageStorage {
    func getFetchRequest(with identifier: String) -> NSFetchRequest<MessageEntity>
    func getEntity(with identifier: String) -> MessageEntity?
    func createEntity() -> MessageEntity
}

class CoreDataMessageStorage: NSObject, IMessageStorage, ICoreDataMessageStorage, ICoreDataStorage {
    weak var delegate: IMessageStorageTableViewDelegate?
    
    typealias CoreDataModel = MessageEntity

    var coreDataStack: ICoreDataStack
    var coreDataMapper: ICoreDataMapper
    
    fileprivate var converationFetchedResultsController: NSFetchedResultsController<CoreDataModel>?

    init(coreDataStack: ICoreDataStack, coreDataMapper: ICoreDataMapper) {
        self.coreDataStack = coreDataStack
        self.coreDataMapper = coreDataMapper
    }

    func get(with identifier: String) -> IMessage? {
        guard let entity = getEntity(with: identifier) else {
            return nil
        }
        return coreDataMapper.map(entity: entity)
    }
    
    func createOrUpdate(model: IMessage) -> IMessage? {
        var entity = getEntity(with: model.identifier) ?? createEntity()
        coreDataMapper.map(model: model, entity: &entity)
        
        
        guard let _ = try? coreDataStack.viewContext.save() else {
            return nil
        }
        
        return model
    }
    
    func setConversationForTableView(conversation: IConversation) {
        converationFetchedResultsController =
            coreDataStack.makeFetchedResultsController(
                NSPredicate(format: "conversation.identifier == %@", conversation.identifier),
                [NSSortDescriptor(key: #keyPath(CoreDataModel.date), ascending: true)
            ]
        )
        converationFetchedResultsController?.delegate = self
        try? converationFetchedResultsController?.performFetch()
    }

    func numberOfSections() -> Int {
        return converationFetchedResultsController?.sections?.count ?? 0
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return converationFetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }
    
    func object(at indexPath: IndexPath) -> IMessage? {
        guard let entity = converationFetchedResultsController?.object(at: indexPath) else {
            return nil
        }
        return coreDataMapper.map(entity: entity)
    }

}

extension CoreDataMessageStorage: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.willChangeContent()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        delegate?.contentChanged(didChange: anObject,
                                 at: indexPath,
                                 for: MessageStorageTableViewChangeType(rawValue: type.rawValue)!,
                                 newIndexPath: newIndexPath)
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.didChangeContent()
    }
}
