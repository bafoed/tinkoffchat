//
//  CoreDataUserStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

protocol ICoreDataUserStorage {
    func getFetchRequest(with identifier: String) -> NSFetchRequest<UserInfoEntity>
    func getEntity(with identifier: String) -> UserInfoEntity?
    func createEntity() -> UserInfoEntity
}

class CoreDataUserStorage: ICoreDataUserStorage, IUserStorage, ICoreDataStorage {
    typealias CoreDataModel = UserInfoEntity
    
    var coreDataStack: ICoreDataStack
    var coreDataMapper: ICoreDataMapper

    init(coreDataStack: ICoreDataStack, coreDataMapper: ICoreDataMapper) {
        self.coreDataStack = coreDataStack
        self.coreDataMapper = coreDataMapper
    }
    
    // MARK: - Protocol implementation
    func get(with identifier: String) -> IUser? {
        guard let entity = getEntity(with: identifier) else {
            return nil
        }
        return coreDataMapper.map(entity: entity)
    }
    
    func createOrUpdate(model: IUser) -> IUser? {
        var entity = getEntity(with: model.identifier) ?? createEntity()
        coreDataMapper.map(model: model, entity: &entity)
        guard let _ = try? coreDataStack.viewContext.save() else {
            return nil
        }
        
        return model
    }
}
