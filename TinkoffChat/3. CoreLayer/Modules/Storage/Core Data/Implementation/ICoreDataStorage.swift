//
//  ICoreDataStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

// Протокол для хранения моделей в CoreData
protocol ICoreDataStorage {
    associatedtype CoreDataModel where CoreDataModel: NSManagedObject

    var coreDataStack: ICoreDataStack { get }
    var coreDataMapper: ICoreDataMapper { get }

    func getFetchRequest(with identifier: String) -> NSFetchRequest<CoreDataModel>
    func getEntity(with identifier: String) -> CoreDataModel?
    func createEntity() -> CoreDataModel
}

extension ICoreDataStorage {
    func getFetchRequest(with identifier: String) -> NSFetchRequest<CoreDataModel> {
        let predicate = NSPredicate(format: "identifier == %@", identifier)
        return coreDataStack.makeFetchRequest(predicate)
    }

    func getEntity(with identifier: String) -> CoreDataModel? {
        let fetchRequest = getFetchRequest(with: identifier)
        guard
            let entities = try? coreDataStack.viewContext.fetch(fetchRequest),
            let entity = entities.last
        else {
            return nil
        }
        return entity
    }
    
    func createEntity() -> CoreDataModel {
        return NSEntityDescription.insertNewObject(forEntityName: String(describing: CoreDataModel.self),
                                                   into: coreDataStack.viewContext) as! Self.CoreDataModel
    }
}
