//
//  CoreDataMapper.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

protocol ICoreDataMapper {
    func map(entity: UserInfoEntity) -> IUser?
    func map(model: IUser, entity: inout UserInfoEntity)
    
    func map(entity: MessageEntity) -> IMessage?
    func map(model: IMessage, entity: inout MessageEntity)
    
    func map(entity: ConversationEntity) -> IConversation?
    func map(model: IConversation, entity: inout ConversationEntity)
}

class CoreDataMapper: ICoreDataMapper {
    func map(entity: UserInfoEntity) -> IUser? {
        guard
            let id = entity.identifier,
            let name = entity.name
        else { return nil }
        
        var avatar: UIImage? = nil
        if let avatarFilename = entity.avatar {
            let avatarURL = getDocumentsDirectory().appendingPathComponent(avatarFilename)
            
            if let data = try? Data(contentsOf: avatarURL) {
                avatar = UIImage(data: data)
            }
        }
        
        return User(identifier: id,
                    name: name,
                    about: entity.about,
                    avatar: avatar)
    }
    
    func map(model: IUser, entity: inout UserInfoEntity) {
        entity.identifier = model.identifier
        entity.name = model.name
        entity.about = model.about
        
        if let avatar = model.avatar {
            let avatarFilename = model.identifier + "_avatar"
            let avatarURL = getDocumentsDirectory().appendingPathComponent(avatarFilename)
            if let data = UIImageJPEGRepresentation(avatar, 1.0), let _ = try? data.write(to: avatarURL) {
                entity.avatar = avatarFilename
            }
        }
    }

    func map(entity: MessageEntity) -> IMessage? {
        guard
            let id = entity.identifier,
            let text = entity.messageText,
            let direction = MessageDirection(rawValue: entity.direction ?? ""),
            let type = MessageType(rawValue: entity.type ?? ""),
            let date = entity.date
        else { return nil }
        
        return Message(identifier: id,
                       text: text,
                       direction: direction,
                       type: type,
                       date: date as Date)
    }
    
    func map(model: IMessage, entity: inout MessageEntity) {
        entity.identifier = model.identifier
        entity.messageText = model.messageText
        entity.direction = model.direction.rawValue
        entity.type = model.type.rawValue
        entity.date = model.date as NSDate
    }
    
    func map(entity: ConversationEntity) -> IConversation? {
        guard
            let id = entity.identifier
        else { return nil }
        
        return Conversation(identifier: id,
                            online: entity.online,
                            hasUnreadMessages: entity.hasUnreadMessages)
    }
    
    func map(model: IConversation, entity: inout ConversationEntity) {
        entity.identifier = model.identifier
        entity.online = model.online
        entity.hasUnreadMessages = model.hasUnreadMessages
    }
    
    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
