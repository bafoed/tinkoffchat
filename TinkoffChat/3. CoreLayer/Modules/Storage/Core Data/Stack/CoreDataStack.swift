//
//  CoreDataStack.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import CoreData


protocol ICoreDataStack {
    var viewContext: NSManagedObjectContext { get }
    var backgroundViewContext: NSManagedObjectContext { get }
    
    func makeFetchRequest<T: NSManagedObject>(_ predicate: NSPredicate?, _ sortDescriptors: [NSSortDescriptor]) -> NSFetchRequest<T>
    func makeFetchRequest<T: NSManagedObject>(_ predicate: NSPredicate?) -> NSFetchRequest<T>
    func makeFetchedResultsController<T: NSManagedObject>(_ fetchRequest: NSFetchRequest<T>) -> NSFetchedResultsController<T>
    func makeFetchedResultsController<T: NSManagedObject>(_ predicate: NSPredicate?, _ sortDescriptors: [NSSortDescriptor]) -> NSFetchedResultsController<T>

}

extension ICoreDataStack {
    func makeFetchRequest<T>(_ predicate: NSPredicate?, _ sortDescriptors: [NSSortDescriptor]) -> NSFetchRequest<T> where T : NSManagedObject {
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest<T>(entityName: String(describing: T.self))
        if let predicate = predicate { fetchRequest.predicate = predicate }
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.resultType = .managedObjectResultType
        return fetchRequest
    }
    
    func makeFetchRequest<T>(_ predicate: NSPredicate?) -> NSFetchRequest<T> where T : NSManagedObject {
        return makeFetchRequest(predicate, [])
    }
    
    func makeFetchedResultsController<T: NSManagedObject>(_ fetchRequest: NSFetchRequest<T>) -> NSFetchedResultsController<T> {
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                    managedObjectContext: viewContext,
                                                    sectionNameKeyPath: nil,
                                                    cacheName: nil)

        return controller
    }
    
    func makeFetchedResultsController<T: NSManagedObject>(_ predicate: NSPredicate?, _ sortDescriptors: [NSSortDescriptor]) -> NSFetchedResultsController<T> {
        let fetchRequest: NSFetchRequest<T> = makeFetchRequest(predicate, sortDescriptors)
        return makeFetchedResultsController(fetchRequest)
    }
}

class CoreDataStack: ICoreDataStack {
    var viewContext: NSManagedObjectContext
    var backgroundViewContext: NSManagedObjectContext
    fileprivate let coreData = CoreDataWithoutPersistantContainerStack()
    
    init() {
        self.viewContext = coreData.managedObjectContext
        self.backgroundViewContext = coreData.backgroundManagedObjectContext
    }
}

// MARK: - Core Data Stack
fileprivate class CoreDataWithoutPersistantContainerStack {
    var errorHandler: (Error) -> Void = {_ in }
    
    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(mainContextChanged(notification:)),
                                               name: .NSManagedObjectContextDidSave,
                                               object: self.managedObjectContext)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(bgContextChanged(notification:)),
                                               name: .NSManagedObjectContextDidSave,
                                               object: self.backgroundManagedObjectContext)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    lazy var libraryDirectory: NSURL = {
        let urls = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "TinkoffChat", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel:
            self.managedObjectModel)
        let url = self.libraryDirectory.appendingPathComponent("TinkoffChat.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                               configurationName: nil,
                                               at: url,
                                               options: [
                                                NSMigratePersistentStoresAutomaticallyOption: true,
                                                NSInferMappingModelAutomaticallyOption: true
                ])
        } catch {
            self.errorHandler(error)
        }
        return coordinator
    }()
    
    lazy var backgroundManagedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var privateManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateManagedObjectContext.persistentStoreCoordinator = coordinator
        return privateManagedObjectContext
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainManagedObjectContext.persistentStoreCoordinator = coordinator
        return mainManagedObjectContext
    }()
    
    @objc func mainContextChanged(notification: NSNotification) {
        backgroundManagedObjectContext.perform { [unowned self] in
            self.backgroundManagedObjectContext.mergeChanges(fromContextDidSave: notification as Notification)
        }
    }
    
    @objc func bgContextChanged(notification: NSNotification) {
        managedObjectContext.perform{ [unowned self] in
            self.managedObjectContext.mergeChanges(fromContextDidSave: notification as Notification)
        }
    }
}
