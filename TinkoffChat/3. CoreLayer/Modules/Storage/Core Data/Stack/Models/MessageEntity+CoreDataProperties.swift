//
//  MessageEntity+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by bf on 11/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//
//

import Foundation
import CoreData


extension MessageEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MessageEntity> {
        return NSFetchRequest<MessageEntity>(entityName: "MessageEntity")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var direction: String?
    @NSManaged public var identifier: String?
    @NSManaged public var messageText: String?
    @NSManaged public var type: String?
    @NSManaged public var conversation: ConversationEntity?

}
