//
//  UserInfoEntity+CoreDataProperties.swift
//  TinkoffChat
//
//  Created by bf on 11/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//
//

import Foundation
import CoreData


extension UserInfoEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserInfoEntity> {
        return NSFetchRequest<UserInfoEntity>(entityName: "UserInfoEntity")
    }

    @NSManaged public var about: String?
    @NSManaged public var avatar: String?
    @NSManaged public var identifier: String?
    @NSManaged public var name: String?
    @NSManaged public var conversation: ConversationEntity?

}
