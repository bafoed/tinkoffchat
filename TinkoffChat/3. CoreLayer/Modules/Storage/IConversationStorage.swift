//
//  IConversationStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import CoreData

enum ConversationStorageTableViewChangeType: UInt {
    case insert = 1
    case delete = 2
    case move = 3
    case update = 4
}

protocol IConversationStorageTableViewDelegate: class {
    func willChangeContent()
    func contentChanged(didChange anObject: Any,
                        at indexPath: IndexPath?,
                        for type: ConversationStorageTableViewChangeType,
                        newIndexPath: IndexPath?)
    func didChangeContent()
}

protocol IConversationStorageConversationOnlineDelegate: class {
    func didBecomeOnline()
    func didBecomeOffline()
}

protocol IConversationStorage {
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func object(at indexPath: IndexPath) -> IConversation?
    var delegate: IConversationStorageTableViewDelegate? { get set }

    func get(with identifier: String) -> IConversation?
    func get(for user: IUser) -> IConversation?
    func createOrUpdate(model: IConversation, user: IUser) -> IConversation?
    
    func addMessage(message: IMessage, to conversation: IConversation)
    func getLatestMessage(in conversation: IConversation) -> IMessage?
    func getUser(in conversation: IConversation) -> IUser?
    func updateReadStatus(conversation: IConversation, read: Bool)
    
    func setConversationForOnlineDelegate(conversation: IConversation)
    var onlineDelegate: IConversationStorageConversationOnlineDelegate? { get set }
    
    func markAllOffline()
}
