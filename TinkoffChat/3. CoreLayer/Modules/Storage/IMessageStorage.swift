//
//  IMessageStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

enum MessageStorageTableViewChangeType: UInt {
    case insert = 1
    case delete = 2
    case move = 3
    case update = 4
}

protocol IMessageStorageTableViewDelegate: class {
    func willChangeContent()
    func contentChanged(didChange anObject: Any,
                        at indexPath: IndexPath?,
                        for type: MessageStorageTableViewChangeType,
                        newIndexPath: IndexPath?)
    func didChangeContent()
}

protocol IMessageStorage {
    func numberOfSections() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func object(at indexPath: IndexPath) -> IMessage?
    var delegate: IMessageStorageTableViewDelegate? { get set }
    
    func setConversationForTableView(conversation: IConversation)
    
    func get(with identifier: String) -> IMessage?
    func createOrUpdate(model: IMessage) -> IMessage?
}
