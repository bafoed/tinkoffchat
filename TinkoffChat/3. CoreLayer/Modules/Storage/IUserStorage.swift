//
//  IUserStorage.swift
//  TinkoffChat
//
//  Created by bf on 18/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IUserStorage {
    func get(with identifier: String) -> IUser?
    func createOrUpdate(model: IUser) -> IUser?
}
