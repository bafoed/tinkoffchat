//
//  Conversation.swift
//  TinkoffChat
//
//  Created by bf on 05/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation


protocol IConversation {
    var identifier: String { get }
    var online: Bool { get set }
    var hasUnreadMessages: Bool { get set }
}

struct Conversation: IConversation {
    var identifier: String
    var online: Bool
    var hasUnreadMessages: Bool
    
    init(identifier: String, online: Bool, hasUnreadMessages: Bool) {
        self.identifier = identifier
        self.online = online
        self.hasUnreadMessages = hasUnreadMessages
    }
    
    static func == (lhs: Conversation, rhs: Conversation) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    static func generateConversationId() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }

}
