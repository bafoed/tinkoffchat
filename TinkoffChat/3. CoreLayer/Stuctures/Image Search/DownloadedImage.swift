//
//  DownloadedImage.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

protocol IDownloadedImage {
    var image: UIImage { get }
}

struct DownloadedImage: IDownloadedImage {
    var image: UIImage
    
    init(image: UIImage) {
        self.image = image
    }
}
