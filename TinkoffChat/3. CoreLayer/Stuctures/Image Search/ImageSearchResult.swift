//
//  ImageSearchResult.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IImageSearchResult {
    var preview: IRemoteImage { get }
    var full: IRemoteImage { get }
}

struct ImageSearchResult: IImageSearchResult {
    var preview: IRemoteImage
    var full: IRemoteImage
    
    init(preview: IRemoteImage, full: IRemoteImage) {
        self.preview = preview
        self.full = full
    }
}
