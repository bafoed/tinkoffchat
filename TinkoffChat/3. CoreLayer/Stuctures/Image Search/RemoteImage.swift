//
//  RemoteImage.swift
//  TinkoffChat
//
//  Created by bf on 25/11/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

protocol IRemoteImage {
    var imageURL: URL { get }
}

struct RemoteImage: IRemoteImage {
    var imageURL: URL
    
    init(imageURL: URL) {
        self.imageURL = imageURL
    }
}
