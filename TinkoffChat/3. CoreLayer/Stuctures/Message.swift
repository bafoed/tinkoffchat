//
//  Message.swift
//  TinkoffChat
//
//  Created by bf on 05/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

enum MessageDirection: String {
    case sent, received
}

enum MessageType: String, Encodable {
    case textMessage = "TextMessage"
}

protocol IMessage {
    var identifier: String { get }
    var messageText: String { get }
    var direction: MessageDirection { get }
    var type: MessageType { get }
    var date: Date { get }
    
    func encodeToJSON() -> Data?
}

struct Message: IMessage, Encodable {
    enum CodingKeys: String, CodingKey {
        case type = "eventType"
        case identifier = "messageId"
        case messageText = "text"
    }
    
    var identifier: String
    var messageText: String
    var direction: MessageDirection
    var type: MessageType
    var date: Date

    init(identifier: String, text: String, direction: MessageDirection, type: MessageType, date: Date) {
        self.identifier = identifier
        self.messageText = text
        self.direction = direction
        self.type = type
        self.date = date
    }

    func encodeToJSON() -> Data? {
        return try? JSONEncoder().encode(self)
    }
    
    static func generateMessageId() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }
}
