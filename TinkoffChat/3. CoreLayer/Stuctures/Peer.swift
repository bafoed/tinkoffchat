//
//  Peer.swift
//  TinkoffChat
//
//  Created by bf on 28/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation

struct Peer {
    var identifier: String
    var name: String
    
    init(identifier: String, name: String) {
        self.identifier = identifier
        self.name = name
    }
    
    static func == (lhs: Peer, rhs: Peer) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
