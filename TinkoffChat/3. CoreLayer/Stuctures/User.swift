//
//  User.swift
//  TinkoffChat
//
//  Created by bf on 21/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import Foundation
import UIKit

protocol IUser {
    var identifier: String { get set }
    var name: String { get set }
    var about: String? { get set }
    var avatar: UIImage? { get set }
    
    static func createDefaultUser() -> IUser
}

struct User: IUser {
    var identifier: String
    var name: String
    var about: String?
    var avatar: UIImage?
    
    init(identifier: String, name: String, about: String?, avatar: UIImage?) {
        self.identifier = identifier
        self.name = name
        self.about = about
        self.avatar = avatar
    }
    
    static func createDefaultUser() -> IUser {
        return User(identifier: "",
                    name: "Tinkoff Chat User",
                    about: nil,
                    avatar: UIImage(named: "UserPlaceholder")!)
    }
}
