//
//  AppDelegate.swift
//  TinkoffChat
//
//  Created by bf on 20/09/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    private let rootAssembly = RootAssembly()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        self.window = UIWindow(frame: UIScreen.main.bounds)
        let controller = rootAssembly.presentationAssembly.conversationListViewController()
        let navController = UINavigationController(rootViewController: controller)
        navController.navigationBar.prefersLargeTitles = true

        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        // UIWindow (а не UIViewController) выбран, чтобы анимация могла работать
        // поверх любых элементов (например поверх UINavigationBar в UINavigationController)
        window?.addGestureRecognizer(rootAssembly.presentationAssembly.tinkoffGestureRecognizer())

        rootAssembly.coreAssembly.conversationStorage.markAllOffline() // отмечаем все диалоги оффлайн
        rootAssembly.serviceAssembly.communcationService.start() // запускаем Multipeer Connectivity
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        rootAssembly.serviceAssembly.communcationService.stop()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        rootAssembly.serviceAssembly.communcationService.start()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

