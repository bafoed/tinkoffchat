//
//  ArrayRandom.swift
//  TinkoffChat
//
//  Created by bf on 04/10/2018.
//  Copyright © 2018 bf. All rights reserved.
//

import GameKit

extension Array {
    func random(seed: Int?) -> Element? {
        if isEmpty { return nil }
        
        let randomSource: GKMersenneTwisterRandomSource
        if seed != nil {
            randomSource = GKMersenneTwisterRandomSource(seed: UInt64(abs(seed!)))
        } else {
            randomSource = GKMersenneTwisterRandomSource()
        }
        
        let randomIndex = GKRandomDistribution(randomSource: randomSource, lowestValue: 0, highestValue: count - 1).nextInt()
        return self[randomIndex]
    }
}
